/* The screen that lets users add items to their virtual wardrobe.
 * Each item has a name, a type (Shirt, Pants, Jacket, Belt, or Accessory), a color, and an image file associated with it.
 * The image file is a fabric sample taken using the phone's camera. The Color is that fabric sample's average color.
 * Code to get an image from the camera adapted from http://mobile.tutsplus.com/tutorials/android/capture-and-crop-an-image-with-the-device-camera/
 * Code to set up ExpandableList adapted from http://www.androidhive.info/2013/07/android-expandable-list-view-tutorial/
 */

package com.example.cameraget;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import android.os.Bundle;
import android.os.Environment;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

import android.view.View;
import android.view.Menu;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class AddClothingActivity extends Activity implements OnClickListener{

	private static final String JPEG_FILE_PREFIX = "IMG_";
	private static final String JPEG_FILE_SUFFIX = ".jpg";
	
	//Fiels used in constructing the Expandable List, from which the clothing type is selected
    ExpandableListView clothinglistView;
    ExpandableListAdapter listAdapter;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
    
	//Variable where the file path of the image is stored
	private String mCurrentPhotoPath;
	final int CAMERA_CAPTURE = 1;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_clothing);
		
		//The button which takes the photo
		Button captureBtn = (Button)findViewById(R.id.capture_btn);
		captureBtn.setOnClickListener(this);
		
		//The button which saves an item
		Button saveBtn = (Button)findViewById(R.id.save_btn);
		saveBtn.setOnClickListener(this);
		
		//Populate the Expandable List
		initList(savedInstanceState);
		
		//If restoring a saved state, replace the mCurrentPhotoPath string to prevent null pointer exceptions
        if(savedInstanceState!=null){
            mCurrentPhotoPath = savedInstanceState.getString("photo");
        }
	}
	
	//Save the mCurrentPhotoPath string
	@Override
	protected void onSaveInstanceState(Bundle outState) {
	    super.onSaveInstanceState(outState);
	    outState.putString("photo", mCurrentPhotoPath);
	    
	}
	
	@Override
	protected void onResume(){
		super.onResume();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		if(v.getId() ==  R.id.capture_btn){
			try{
				//Creating the intent that'll get the picture (using the built-in camera app)
				Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
				
				//Check if external storage is available for read/write
			    String state = Environment.getExternalStorageState();
			    if (Environment.MEDIA_MOUNTED.equals(state)) {
			       Log.d("Message","Media mounted");
			    }
			    
			    //Get the picture
			    File f = null;
			    try{
			    	f = setUpPhotoFile();//Setup the File
			    	mCurrentPhotoPath = f.getAbsolutePath();//Saves that File as a string
//			    	Toast toast = Toast.makeText(this, mCurrentPhotoPath, Toast.LENGTH_LONG);
//			    	toast.show();
					captureIntent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));//Tells the captureIntent to save the image at the designated file path
			    }catch(IOException e){
			    	
			    }
			    
			    //Start up the intent. When this finishes running, onActivityResult() will be called
				startActivityForResult(captureIntent, CAMERA_CAPTURE);
				
			}catch(ActivityNotFoundException anfe){
				String errorMessage = "Whoops - your device doesn't support capturing images!";
				Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_LONG);
				toast.show();
			}
		}else if(v.getId() == R.id.save_btn){
			//Get String values from the Views
			EditText clothingNameText = (EditText)findViewById(R.id.ClothingName);
			String clothingName = clothingNameText.getText().toString();
			EditText clothingTypeText = (EditText)findViewById(R.id.ClothingTypeTitle);
			String clothingType = clothingTypeText.getText().toString();
			TextView avgColText = (TextView)findViewById(R.id.average_color);
			String averageColor = avgColText.getText().toString();
			
			//Toast.makeText(this, averageColor, Toast.LENGTH_LONG).show();
			
			if(clothingName.length()==0 || mCurrentPhotoPath.length()==0 || averageColor.equals("Average Color")){
				Toast.makeText(this, "Please fill all fields", Toast.LENGTH_SHORT).show();
				return;
			}
			
		    try
		    {
		    	//Write the String values to the appropriate text file
		    	//These are stored in data/data/com.example.cameraget/files
		    	File file = getFileStreamPath(clothingType.trim()+".txt");
		    	String filePath = file.getPath();
		    	
		    	if (!file.exists()) {
		    	   file.createNewFile();
		    	}

		    	FileOutputStream output = openFileOutput(clothingType.trim()+".txt", Context.MODE_PRIVATE);
		    	//FileOutputStream output = new FileOutputStream(filePath);
		    	Log.d("Output path", filePath);
		    	
		    	output.write(clothingName.getBytes());
		    	output.write(System.getProperty("line.separator").getBytes());
		    	output.write(clothingType.getBytes());
		    	output.write(System.getProperty("line.separator").getBytes());
		    	output.write(averageColor.getBytes());
		    	output.write(System.getProperty("line.separator").getBytes());
		    	output.write(mCurrentPhotoPath.getBytes());
		    	output.write(System.getProperty("line.separator").getBytes());
		    	
		    	output.close();
		    	
				Toast toast = Toast.makeText(this, clothingName + " Saved: " , Toast.LENGTH_LONG);
				toast.show();
				
				//Clear all fields after saving so no duplicate items are entered
				mCurrentPhotoPath = "";
				ImageView imgView = (ImageView)findViewById(R.id.picture_cropped);
				imgView.setImageBitmap(null);
				TextView nameView = (TextView)findViewById(R.id.ClothingName);
				nameView.setText("");
				TextView colorView = (TextView)findViewById(R.id.average_color);
				colorView.setText("Average Color");
				colorView.setBackgroundColor(Color.WHITE);
		    }
		    catch (Exception ex)
		    {
		        Toast toast = Toast.makeText(this,  "Error saving file " + ex.getLocalizedMessage(), Toast.LENGTH_LONG);
		    	toast.show();
		    }
	
		

		}
	}
	
	//Called on returning from the Camera
	protected void onActivityResult(int requestCode, int resultCode, Intent data){
		if(resultCode == RESULT_OK){
			if(requestCode == CAMERA_CAPTURE){
				//picUri = data.getData();

				/* Get the size of the ImageView */
				int targetW = 300;
				int targetH = 200;

				/* Get the size of the image */
				BitmapFactory.Options bmOptions = new BitmapFactory.Options();
				BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
				int photoW = bmOptions.outWidth;
				int photoH = bmOptions.outHeight;
				/* Figure out which way needs to be reduced less */
				int scaleFactor = 1;
				if ((targetW > 0) || (targetH > 0)) {
					scaleFactor = Math.min(photoW/targetW, photoH/targetH);	
				}

				/* Set bitmap options to scale the image decode target */
				bmOptions.inJustDecodeBounds = false;
				bmOptions.inSampleSize = scaleFactor;
				bmOptions.inPurgeable = true;

				/* Decode the JPEG file into a Bitmap */
				Bitmap pic = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);

				if(pic!=null){
					//Crop the image
					Bitmap croppedPic = Bitmap.createBitmap(pic, 0, pic.getHeight()/2-25, pic.getWidth(), 50);
					ImageView croppedPicView = (ImageView)findViewById(R.id.picture_cropped);
					croppedPicView.setImageBitmap(croppedPic);//Set the second ImageView to the cropped image
					Log.d("Crop the pic", "Crop the pic");
					
					//Determine the average color of the preview image, display it in the TextView
					int averageColor = getAverageColor(croppedPic);
					TextView averageColorText = (TextView)findViewById(R.id.average_color);
					averageColorText.setBackgroundColor(averageColor);
					//averageColorText.setText(toRGB(averageColor));
					averageColorText.setText(""+averageColor);
					//Should probably close the images properly when you're done to save memory
				}else{
					Toast.makeText(this,  "Please save photo in vertical orientation", Toast.LENGTH_LONG).show();
				}
			}
		}
	}
	
	//Turns a hex argb number like #FF00FF22 into the corresponding RGB values like 0:255:34
    public static String toRGB(int argb)
    {
        int r = ((argb >> 16) & 0xff);
        int g = ((argb >> 8) & 0xff);
        int b = (argb & 0xff);

        return String.format("" + r + ":" + g + ":" + b);
    }
    
    //Sums up the colors of all the pixels of an image and gets an average RGB value
	private int getAverageColor(Bitmap b){
		int imgSize = b.getWidth()*b.getHeight();
		int[] pixelArr = new int[imgSize];
		b.getPixels(pixelArr, 0, b.getWidth(), 0, 0, b.getWidth(), b.getHeight());
		
		//Assuming total opacity aka Alpha = FF all the time
		int redSum = 0;
		int greenSum = 0;
		int blueSum = 0;

		for(int i=0; i<pixelArr.length; i++){
			redSum +=(pixelArr[i] & 0x00FF0000)>>16;
			greenSum +=(pixelArr[i] & 0x0000FF00)>>8;
			blueSum +=(pixelArr[i] & 0x000000FF);
		}
		
		return Color.argb(125, redSum/imgSize, greenSum/imgSize, blueSum/imgSize);
	}
	
	//////////////////////////////File IO Stuff Here, taken from internet tutorial///////////////////////////////////////////////////////////////////////////////
	
	/* Get the photo album name you've set for this application */
	private String getAlbumName() {
		return getString(R.string.album_name);
	}
	
	//Setup a File pointing to the default pictures directory on the sd card with the album name that you specify
	public File getAlbumStorageDir(String albumName) {
		// TODO Auto-generated method stub
		return new File(
		  Environment.getExternalStoragePublicDirectory(
		    Environment.DIRECTORY_PICTURES
		  ), 
		  albumName
		);
	}
	
	//Setup up the File to point to the directory you want
	private File getAlbumDir() {
		File storageDir = null;

		if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
			
			//Here's where you set it up with the above two methods
			storageDir = getAlbumStorageDir(getAlbumName());

			if (storageDir != null) {
				if (! storageDir.mkdirs()) {
					if (! storageDir.exists()){
						Log.d("CameraSample", "failed to create directory");
						return null;
					}
				}
			}
		
		} else {
			Log.v(getString(R.string.app_name), "External storage is not mounted READ/WRITE.");
		}
		
		return storageDir;
	}

	//Makes the full Filepath, using the File which points to the image's album and a specified file name for the image itself
	private File createImageFile() throws IOException {
		// Create an image file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
		String imageFileName = JPEG_FILE_PREFIX + timeStamp + "_";//Looks like "IMG_20131205_214355_"
		File albumF = getAlbumDir();//Looks like "images/Clothing" or something (just for illustration purposes)
		File imageF = File.createTempFile(imageFileName, JPEG_FILE_SUFFIX, albumF);//Looks like "images/Clothing/IMG_20131205_214355.jpg"
		return imageF;
	}

	//Gets called in onClick(), calls createImageFile() to generate the File
	private File setUpPhotoFile() throws IOException {
		File f = createImageFile();
		mCurrentPhotoPath = f.getAbsolutePath();//Might be redundant since we set mCurrentPhotoPath in onClick() as well
		return f;
	}
	
	//////////////////////////End File IO Stuff///////////////////////////////////////////////////////////////////////////////////
	
	/////////////////////////List Stuff////////////////////////////////////////////////////////////////////////////////////////////

	void initList(Bundle savedInstanceState) {
	clothinglistView = (ExpandableListView)findViewById(R.id.ClothingListView);
	prepareListData();

    listAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild);

    // setting list adapter
    clothinglistView.setAdapter(listAdapter);
    clothinglistView.setOnChildClickListener(new OnChildClickListener() {
    	 
        @Override
        public boolean onChildClick(ExpandableListView parent, View v,
                int groupPosition, int childPosition, long id) {
			EditText clothingTypeText = (EditText)findViewById(R.id.ClothingTypeTitle);
			clothingTypeText.setText(listDataChild.get(listDataHeader.get(groupPosition)).get(childPosition));
			
			//Toast.makeText(getApplicationContext(), ""+groupPosition + " " + childPosition, Toast.LENGTH_SHORT).show();			
			
//            Toast.makeText(
//                    getApplicationContext(),
//                    listDataHeader.get(groupPosition)
//                            + " : "
//                            + listDataChild.get(
//                                    listDataHeader.get(groupPosition)).get(
//                                    childPosition), Toast.LENGTH_LONG)
//                    .show();
            return false;
        }
    });

	}

    /*
     * Preparing the list data
     */
    private void prepareListData() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();
 
        // Adding child data
        listDataHeader.add("Press here to select clothing type");
 
        // Adding child data
        List<String> clothingTypes = new ArrayList<String>();
        clothingTypes.add("Shirt");
        clothingTypes.add("Pants");
        clothingTypes.add("Jacket");
        clothingTypes.add("Belt");
        clothingTypes.add("Accessory");
 
        listDataChild.put(listDataHeader.get(0), clothingTypes); // Header, Child data
    }

    ////////////////////////End List Stuff///////////////////////////////////////////////////////////////////////////////////////
    
}

