/* The screen that lets users assemble one item of each type of clothing - Shirt, Pants, Jacket, Belt, and Accessory into an outfit
 * Then the user presses Does This Match? and the app calculates if the selected clothing items "match"
 * Code to set up ExpandableList adapted from http://www.androidhive.info/2013/07/android-expandable-list-view-tutorial/
 */

package com.example.cameraget;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ExpandableListView.OnChildClickListener;

public class BuildOutfitActivity extends Activity implements OnClickListener{
    
	ExpandableListView clothinglistView;
    String ClothingType;
    
    //Lists for filling out the ExpandableList
    ArrayList<String> shirtNames;
    ArrayList<String> shirtColors;
    ArrayList<String> shirtPhotoPaths;
    ArrayList<String> pantsNames;
    ArrayList<String> pantsColors;
    ArrayList<String> pantsPhotoPaths;
    ArrayList<String> jacketNames;
    ArrayList<String> jacketColors;
    ArrayList<String> jacketPhotoPaths;
    ArrayList<String> beltNames;
    ArrayList<String> beltColors;
    ArrayList<String> beltPhotoPaths;
    ArrayList<String> accNames;
    ArrayList<String> accColors;
    ArrayList<String> accPhotoPaths;
    
    int[] colors;
    
    ExpandableListAdapter listAdapter;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_build_outfit);
		
		colors = new int[5];
		for(int i=0; i<colors.length; i++)//Set the colors array to flag values which mean "nothing currently selected"
			colors[i] = -99;
		
		Button button = (Button)findViewById(R.id.query_btn);//The "Does This Match?" button
		button.setOnClickListener(this);
		
		try {
			initFields();//Fills the arraylists with values from the text files
			initClothesList();//Generates the ExpandableList using the arraylists
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	@Override
	protected void onResume(){
		super.onRestart();
		try {
			initFields();
			initClothesList();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	/////////////////////////List Stuff////////////////////////////////////////////////////////////////////////////////////////////

	//Initialize the arraylists and populate them using the text files
	void initFields() throws IOException{
	    shirtNames = new ArrayList<String>();
	    shirtColors = new ArrayList<String>();
	    shirtPhotoPaths = new ArrayList<String>();
	    
	    pantsNames = new ArrayList<String>();
	    pantsColors = new ArrayList<String>();
	    pantsPhotoPaths = new ArrayList<String>();
	    
	    jacketNames = new ArrayList<String>();
	    jacketColors = new ArrayList<String>();
	    jacketPhotoPaths = new ArrayList<String>();
	    
	    beltNames = new ArrayList<String>();
	    beltColors = new ArrayList<String>();
	    beltPhotoPaths = new ArrayList<String>();
	    
	    accNames = new ArrayList<String>();
	    accColors = new ArrayList<String>();
	    accPhotoPaths = new ArrayList<String>();
	    
	    fillArrayList(shirtNames, shirtColors, shirtPhotoPaths, "Shirt.txt");
	    fillArrayList(pantsNames, pantsColors, pantsPhotoPaths, "Pants.txt");
	    fillArrayList(jacketNames, jacketColors, jacketPhotoPaths, "Jacket.txt");
	    fillArrayList(beltNames, beltColors, beltPhotoPaths, "Belt.txt");
	    fillArrayList(accNames, accColors, accPhotoPaths, "Accessory.txt");
	    
	    //Toast.makeText(getApplicationContext(), ""+shirtNames.size()+" "+beltNames.size(), Toast.LENGTH_LONG).show();		
	}
	
	//Fill a group of arraylists with the values from a specified text file
	void fillArrayList(ArrayList<String> namesList, ArrayList<String> colorsList, ArrayList<String> pathsList, String filename) throws IOException{
    	File file = getFileStreamPath(filename);

    	if (!file.exists())
    	{
    	   //file.delete();
    	   file.createNewFile();
    	}

		//File file = getFileStreamPath(filename);
		//Log.d(file.getPath(), file.getPath());
		//Log.d(file.getAbsolutePath(), file.getAbsolutePath());
		//String filePath = file.getPath();
        FileInputStream inputStream = this.openFileInput(filename);
        //FileInputStream inputStream = new FileInputStream(filePath);
		BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

        String line = null, input="";
        while ((line = reader.readLine()) != null){
        	Log.d(line,line);//LOG STATEMENT
            namesList.add(line);
            line = reader.readLine();//Eat the type
            line = reader.readLine();
            colorsList.add(line);
            line = reader.readLine();
            pathsList.add(line);
        }
        reader.close();
//      inputstream.close();
        return;
	}
	
	//Sets up the ExpandableList
	void initClothesList() throws IOException {
		clothinglistView = (ExpandableListView)findViewById(R.id.WardrobeListView);
		prepareListData();
	
	    listAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild);
	
	    // setting list adapter
	    clothinglistView.setAdapter(listAdapter);
	    clothinglistView.setOnChildClickListener(new OnChildClickListener() {
	    	 
	        @Override
	        public boolean onChildClick(ExpandableListView parent, View v,
	                int groupPosition, int childPosition, long id) {
	        	
	        	switch(groupPosition){
	        	case 0://shirt
	        		setSwatch(shirtPhotoPaths.get(childPosition), R.id.shirt_swatch);
	        		setText(shirtColors.get(childPosition), R.id.shirt_text);
		        	colors[groupPosition] = Integer.parseInt(shirtColors.get(childPosition));
	        		break;
	        	case 1://pants
	        		setSwatch(pantsPhotoPaths.get(childPosition), R.id.pants_swatch);
	        		setText(pantsColors.get(childPosition), R.id.pants_text);
	        		colors[groupPosition] = Integer.parseInt(pantsColors.get(childPosition));
	        		break;
	        	case 2://jacket
	        		setSwatch(jacketPhotoPaths.get(childPosition), R.id.jacket_swatch);
	        		setText(jacketColors.get(childPosition), R.id.jacket_text);
	        		colors[groupPosition] = Integer.parseInt(jacketColors.get(childPosition));
	        		break;
	        	case 3://belt
	        		setSwatch(beltPhotoPaths.get(childPosition), R.id.belt_swatch);
	        		setText(beltColors.get(childPosition), R.id.belt_text);
	        		colors[groupPosition] = Integer.parseInt(beltColors.get(childPosition));
	        		break;
	        	case 4://accessory
	        		setSwatch(accPhotoPaths.get(childPosition), R.id.accessory_swatch);
	        		setText(accColors.get(childPosition), R.id.accessory_text);
	        		colors[groupPosition] = Integer.parseInt(accColors.get(childPosition));
	        		break;
	        	}

	        	
				//Toast.makeText(getApplicationContext(), ""+groupPosition + " " + childPosition, Toast.LENGTH_SHORT).show();			
				
	//            Toast.makeText(
	//                    getApplicationContext(),
	//                    listDataHeader.get(groupPosition)
	//                            + " : "
	//                            + listDataChild.get(
	//                                    listDataHeader.get(groupPosition)).get(
	//                                    childPosition), Toast.LENGTH_LONG)
	//                    .show();
	            return false;
	        }
	    });
		
	}

	//Gets the image file of the fabric sample from external storage
	private void setSwatch(String photoPath, int view_id){
		/* Get the size of the ImageView */
		int targetW = 300;
		int targetH = 200;

		/* Get the size of the image */
		BitmapFactory.Options bmOptions = new BitmapFactory.Options();
		BitmapFactory.decodeFile(photoPath, bmOptions);
		int photoW = bmOptions.outWidth;
		int photoH = bmOptions.outHeight;
		/* Figure out which way needs to be reduced less */
		int scaleFactor = 1;
		if ((targetW > 0) || (targetH > 0)) {
			scaleFactor = Math.min(photoW/targetW, photoH/targetH);	
		}

		/* Set bitmap options to scale the image decode target */
		bmOptions.inJustDecodeBounds = false;
		bmOptions.inSampleSize = scaleFactor;
		bmOptions.inPurgeable = true;

		/* Decode the JPEG file into a Bitmap */
		Bitmap pic = BitmapFactory.decodeFile(photoPath, bmOptions);

		//Crop the image
		Bitmap croppedPic = Bitmap.createBitmap(pic, 0, pic.getHeight()/2-25, pic.getWidth(), 50);
		ImageView croppedPicView = (ImageView)findViewById(view_id);
		croppedPicView.setImageBitmap(croppedPic);//Set the second ImageView to the cropped image
	}
	
	//Set the background color of the textview of the clothing item to be the average color of the item
	private void setText(String colorStr, int view_id){
		TextView text = (TextView)findViewById(view_id);
		text.setBackgroundColor(Integer.parseInt(colorStr));
	}
	
	
    /*
     * Preparing the list data
     */
    private void prepareListData() throws IOException {       
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();
 
        // Adding child data
        listDataHeader.add("Shirt");
        listDataHeader.add("Pants");
        listDataHeader.add("Jacket");
        listDataHeader.add("Belt");
        listDataHeader.add("Accessory");
 
        // Adding child data
        listDataChild.put(listDataHeader.get(0), shirtNames); // Header, Child data
        listDataChild.put(listDataHeader.get(1), pantsNames); 
        listDataChild.put(listDataHeader.get(2), jacketNames); 
        listDataChild.put(listDataHeader.get(3), beltNames); 
        listDataChild.put(listDataHeader.get(4), accNames);
    }

    ////////////////////////End List Stuff///////////////////////////////////////////////////////////////////////////////////////
	
    @Override
	public void onClick(View v) {
    	if(v.getId() == R.id.query_btn){
    		
    		//Check pairwise if every item matches every other item
    		boolean doesMatch = true;
    		float[] hsva = new float[3];
    		float[] hsvb = new float[3];
    		for(int i=0; i<colors.length; i++){
    			if(colors[i]>0){
    				Color.colorToHSV(colors[i], hsva);//convert rgb to hsv
	    			for(int j=0; j<colors.length; j++){
	    				if(colors[j]>0){
	    					Color.colorToHSV(colors[j], hsvb);
	    		    		if (hsva[1] <.15) {hsva[0] = hsvb[0];}//if saturation is low, set hue from one color equal to hue of other color.
	    		    		if (hsvb[1] <.15) {hsvb[0] = hsva[0];}//as black and white both match everything
	    		    		if(!match(hsva[0], hsvb[0]))
	    		    			doesMatch = false;
	    				}
	    			}
    			}
    		}

    		//Display the result in a dialog
    		AlertDialog.Builder builder = new AlertDialog.Builder(this);
    		builder.setMessage(doesMatch?"         Yes!":"         No!")
    		       .setCancelable(false)
    		       .setPositiveButton("OK", new DialogInterface.OnClickListener() {
    		           public void onClick(DialogInterface dialog, int id) {
    		        	   dialog.cancel();
    		           }
    		       });
    		AlertDialog answer = builder.create();
    		answer.show();
    	}
	}
    
    //Returns if the clothing items match
    //The values being compared are the hue value of the item, in the hsv color scheme
	public boolean match(float a, float b){
		float c = Math.abs(a-b);
		if (c<=30) {return true;}
		else if ((c >= 85) && (c <= 95)) {return true;}
		else if ((c >= 175) && (c <= 185)) {return true;}
		else if ((c >= 265) && (c <= 255)) {return true;}
		else if ((c >= 330) && (c <= 360)) {return true;}
		else return false;
	}


}