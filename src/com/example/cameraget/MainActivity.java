/**
 * Main Activity of the app, runs when app is started
 * Provides navigation to the two parts of the app: the Clothing Adder and the Outfit Builder
 */

package com.example.cameraget;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends Activity implements OnClickListener{
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		Button addItemBtn = (Button)findViewById(R.id.add_item_btn);
		addItemBtn.setOnClickListener(this);
		
		Button buildOutfitBtn = (Button)findViewById(R.id.build_outfit_btn);
		buildOutfitBtn.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		if(v.getId() ==  R.id.add_item_btn){
			Intent intent = new Intent(this, AddClothingActivity.class);
			startActivity(intent);
		}else if(v.getId() == R.id.build_outfit_btn){
			Intent intent = new Intent(this, BuildOutfitActivity.class);
			startActivity(intent);
		}
	}

}