To compile and run the code using eclipse:
	Configure an android phone emulator or connect an android phone in usbdebugging mode.
	Make sure there is an sd card, and a camera. Program will severely lack functionality without those components.
	Hit run to compile and load the app onto the real or emulated phone


To use the program:
First go to the Add Clothing Item screen to add clothing items. For each item of clothing:
	Take a closeup picture of the clothing item using the camera.
	Type a name for the clothing item
	Select the type of the clothing item from a dropdown menu
	Select save.
Then go back to the main menu, and head into the Build Outfit screen.
	Select a clothing item from each category. This will load them into the outfit stack.
	Hit "Does This Match?" to see if the outfit matches.